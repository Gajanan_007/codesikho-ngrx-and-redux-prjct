import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './+components/+layout/header/header.component';
import { DashboardComponent } from './+components/+layout/dashboard/dashboard.component';
 
import { UsersComponent } from './+components/users/users.component';
import { PostsComponent } from './+components/posts/posts.component';
import { YoutubeLayoutComponent } from './+components/+layout/youtube-layout/youtube-layout.component';
import { HttpService } from './+services/http.service';
import { ApiService } from './+services/api.service';
import { HttpClientModule } from '@angular/common/http';
import { UserListComponent } from './+components/user-list/user-list.component';
import { UserCardComponent } from './+components/user-card/user-card.component';
import { StoreModule } from '@ngrx/store';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    YoutubeLayoutComponent,
    UsersComponent,
    PostsComponent,
    UserListComponent,
    UserCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({}, {})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
