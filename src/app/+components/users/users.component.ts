import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/+models/users';
import { ApiService } from 'src/app/+services/api.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users:User[]=[];
  
  constructor(private ApiService:ApiService) { }
  ngOnInit(): void {
    this.fetchData();
  }
  fetchData(){
    this.ApiService.getAllPost().subscribe((data) => {
       this.users = data;
       console.log(this.users);
    });
  }

}
