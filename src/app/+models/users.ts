export interface User{
id:number;
name:string;
email:string;
address:Address;
username:string;
}

export interface Address{
    street:string;
    suite:string;
    citi:string;
    zipcode:string;
}

export interface Geo{
    lat:string;
    lng:string;
}