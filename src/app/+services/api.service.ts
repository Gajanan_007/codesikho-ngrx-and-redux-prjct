import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { User } from "../+models/users";
//This is custom http service .
import { HttpService } from "./http.service";

@Injectable({
    providedIn:'root'
})

export class ApiService{
    
    constructor(private httpServie:HttpService){}
    getAllPost():Observable<User[]>{
        return this.httpServie.get('/users')
            .pipe(map(data => data as User[])); 
    }
}