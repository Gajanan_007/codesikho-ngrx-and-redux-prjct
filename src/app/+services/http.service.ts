import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn:'root'
})
export class HttpService {
    private baseUrl = "https://jsonplaceholder.typicode.com";

    constructor(private httpClient: HttpClient) {
    }

    get(url: string, params?: any): Observable<any> {
        const data = { params };
        return this.httpClient
            .get(this.baseUrl + url, data)
            .pipe(catchError(this.errorHandlar.bind(this)));
    }

    private errorHandlar(response: any) {
        const error = response.error;
        const keys = Object.keys(error);
        const key = keys[0];
        let message = error[key];

        if(response.status === 401){
            //auth token delete.
            //redirect to login.
        }
        //if error first key contian error object/other array.  
        if (error[key] instanceof Array) {
            message = error[key][0];
        }
        if (key === 'isTrusted') {
            //No internet connection.
        } else {
            message: key + ' : ' + message;
        }

        return throwError({ messages: message, error });
    }
}