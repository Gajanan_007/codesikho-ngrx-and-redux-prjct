import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './+components/+layout/dashboard/dashboard.component';
import { PostsComponent } from './+components/posts/posts.component';
import { UsersComponent } from './+components/users/users.component';

const routes: Routes = [{
  path:'',component:DashboardComponent,
  children:[
    {path:'users',component:UsersComponent},
    {path:'posts',component:PostsComponent}
  ]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
